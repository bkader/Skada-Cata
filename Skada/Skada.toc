## Interface: 40000
## Title: Skada
## Notes: Modular damage meter.
## Notes-deDE: Ein Damage-Meter.
## Notes-esES: Un medidor de daño.
## Notes-esMX: Un medidor de daño.
## Notes-frFR: Un damage meter.
## Notes-koKR: 데미지 미터 모듈입니다.
## Notes-ruRU: Измеритель урона.
## Notes-zhCN: 模块化伤害统计。
## Notes-zhTW: 模組化的傷害統計。
## OptionalDeps: Ace3, LibSharedMedia-3.0, LibWindow-1.1, LibDBIcon-1.0, AceGUI-3.0-SharedMediaWidgets, LibBossIDs-1.0, LibFail-1.0
## LoadOnDemand: 0
## SavedVariables: SkadaDB
## SavedVariablesPerCharacter: SkadaCharDB
## DefaultState: enabled
## Author: Kader (|cff808080bkader#6361|r)
## Version: 1.8.77
## X-Date: 2022-05-12 @ 02:00 PM |cff808080UTC|r
## X-Credits: Zarnivoop
## X-Category: Combat
## X-License: MIT/X
## X-Email: bkader@mail.com
## X-Website: https://github.com/bkader/Skada-Cata
## X-Discord: https://discord.gg/a8z5CyS3eW
## X-Donate: |cff0079c1PayPal|r or |cff246a96Paysera|r at |cff20ff20bkader@mail.com|r

Libs\Libs.xml
Locales\Locales.xml
Core\Core.xml
Modules\Modules.xml